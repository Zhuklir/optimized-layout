# Optimized-layout
* * *

![alt-текст](https://camo.githubusercontent.com/4f54779e24cd26b9d38b6f76d980939049af8a35/687474703a2f2f7777772e6a6f7264616e63726f776e2e636f6d2f77702d636f6e74656e742f75706c6f6164732f323031352f30382f67756c702e6a7067 "Gulp.js")
* * *

Optimized-layout includes everything you need for high-quality and fast website navigation.
Namely the Sass compiler, the local BrowserSync server, the auto-prefixer, the compression of images and scripts.

## How to work with the Optimized Layout?

1. Download the **optimized-layout** with github ;
2. Install Node Modules: **npm i** ;
3. Run the template:  **gulp**.

## Сommands:
1. **gulp build** - the final assembly project
2. **gulp** - launch

