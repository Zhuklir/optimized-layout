const gulp  = require('gulp'),
      sass = require('gulp-sass'),
      cleanCSS = require('gulp-clean-css'),
      browserSync = require('browser-sync'),
      autoprefixer = require('gulp-autoprefixer'),
      cssbeautify = require('gulp-cssbeautify'),
      runSequence = require('run-sequence'),
      imagemin = require('gulp-imagemin'),
      notify = require("gulp-notify"),
      useref = require('gulp-useref'),
      uglify = require('gulp-uglify'),
      gulpIf = require('gulp-if'),
      cache = require('gulp-cache'),
      del = require('del');

//Sass compilation
gulp.task('sass', function(){
    return gulp.src('app/sass/**/*.sass')
    .pipe(sass({includePaths: require('node-bourbon').includePaths}).on('error', sass.logError))
    .pipe(autoprefixer({browsers: ['last 5 versions'],cascade: false}))
    .pipe(cssbeautify())
    //.pipe(cleanCSS({compatibility: 'ie8'}))  optional if necessary 
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
        stream: true
    }))
});


//Server
gulp.task('browserSync', function(){
    browserSync({
        server: {
            baseDir: 'app'
        },
    })
});

//Minimizing JavaScript
gulp.task('useref', function () {
    return gulp.src('app/*.html')
        .pipe(useref())
        //.pipe(gulpIf('*.js', uglify())) Addition of a minification at will. 
        .pipe(gulp.dest('dist'));
});


//Minimizing images
gulp.task('images', function () {
    return gulp.src('app/img/**/*.+(png|jpg|jpeg|gif|svg)')
        // Caching images that ran through imagemin
        .pipe(cache(imagemin({
            interlaced: true,
        })))
        .pipe(gulp.dest('dist/images'))
});


//Fonts
gulp.task('fonts', function(){
    return gulp.src('app/fonts/**/*')
    .pipe(gulp.dest('dist/fonts'))
});





// Clean 
gulp.task('clean', function () {
     return del('dist/**', {force:true});
})


gulp.task('clean:dist', function () {
    return del.sync(['dist/**/*', '!dist/images', '!dist/images/**/*']);
});



gulp.task('default', function (cb) {
    runSequence(['sass','browserSync', 'watch'],
    cb)
 });


// Gulp watch
gulp.task('watch',['browserSync', 'sass'], function(){
    gulp.watch('app/sass/**/*.sass', ['sass']);
    gulp.watch('app/*.html', browserSync.reload);
    gulp.watch('app/js/**/*.js', browserSync.reload);
});


/*BUILD*/

gulp.task('build', ['clean:dist', 'images', 'sass', 'useref'], function () {

    var buildFiles = gulp.src([
        'app/*.html',
    ]).pipe(gulp.dest('dist'));

    var buildCss = gulp.src([
        'app/css/main.css',
    ]).pipe(gulp.dest('dist/css'));

    var buildJs = gulp.src([
        'app/js/common.js',
    ]).pipe(gulp.dest('dist/js'));

    var buildFonts = gulp.src([
        'app/fonts/**/*',
    ]).pipe(gulp.dest('dist/fonts'));

    var buildLibs = gulp.src([
        'app/libs/**/*',
    ]).pipe(gulp.dest('dist/libs'));

});


